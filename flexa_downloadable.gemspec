$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "flexa_downloadable/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "flexa_downloadable"
  s.version     = FlexaDownloadable::VERSION
  s.authors     = ["Gil Gomes"]
  s.email       = ["gilgomesp@gmail.com"]
  s.homepage    = "https://www.flexait.com.br"
  s.summary     = "Downloadable"
  s.description = "Description of FlexaDownloadable."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", ">= 5.0.0.beta3", "< 5.1"
  s.add_dependency 'mini_magick'

  s.add_development_dependency "sqlite3"
end
