class CreateFlexaDownloadableUploads < ActiveRecord::Migration[5.0]
  def change
    create_table :flexa_downloadable_uploads do |t|
      t.string :filename
      t.string :content_type
      t.binary :content

      t.timestamps
    end
  end
end
