require_dependency "flexa_downloadable/application_controller"

module FlexaDownloadable
  class FlexaDownloadable::DownloadsController < ApplicationController
    def download
      file = find_file
      exec(file)
    end

    def by_id
      file = FlexaDownloadable::Upload.find_by(file_params)
      exec(file)
    end

    def image
      file = FlexaDownloadable::Upload.find_by(file_params)
      exec(file, true)
    end

    def find_file
      id = params[:id].to_i
      klass = params[:klass].classify
      field = params[:field].downcase

      object = eval("::#{klass}.find(#{id})")
      object.send(field)
    end

    private

    def exec(file, is_image = false)
      if file
        content = file.content
        content = to_image(content) if is_image
        send_data(
          content,
          filename: file.filename,
          content_type: file.content_type,
          disposition: content_disposition(is_image))
      else
        redirect_to :back
      end
    end

    def content_disposition is_image
      if is_image
        'inline'
      else
        'attachment'
      end
    end

    def to_image content
      Image.new(content, params[:width], params[:height]).resize
    end

    def file_params
      params.permit(:id, :filename)
    end
  end
end
