module FlexaDownloadable
  class Engine < ::Rails::Engine
    isolate_namespace FlexaDownloadable

    initializer "flexa_downloadble", before: :load_and_config_initializers do |app|
      Rails.application.routes.append do
        mount FlexaDownloadable::Engine => "/flexa_downloadable"
      end

      ActiveSupport.on_load :action_controller do
        helper FlexaDownloadable::ApplicationHelper
      end
    end
  end
end
