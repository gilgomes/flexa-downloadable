class Person < ApplicationRecord
  belongs_to :avatar, class_name: FlexaDownloadable::Upload, optional: true
  accepts_nested_attributes_for :avatar
end
