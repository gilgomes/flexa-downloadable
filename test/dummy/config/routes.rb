Rails.application.routes.draw do
  resources :people
  root to: 'welcome#index'
  get 'welcome/index'
end
