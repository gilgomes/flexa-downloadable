module FlexaDownloadable
  class Upload < ApplicationRecord
    after_create :implode!
    before_save :read_file

    private

    def read_file
      if content
        self.filename = content.original_filename
        self.content_type = content.content_type
        self.content = content.read
      end
    end

    def implode!
      destroy if content.nil?
    end
  end
end
