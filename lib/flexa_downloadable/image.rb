module FlexaDownloadable
  class Image
    def initialize image, width = nil, height = nil
      @image = image
      @with = width
      @height = height
    end

    def resize
      if (@width || @height)
        image = MiniMagick::Image.read(@image)
        resized = image.resize("#{@width}x#{@height}")
        resized.to_blob
      else
        @image
      end
    end
  end
end
